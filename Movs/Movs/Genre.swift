//
//  Genre.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 09/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import ObjectMapper

class Genre: Mappable {

    var id: Int?
    var name: String?

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
    }

}
