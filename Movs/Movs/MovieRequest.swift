//
//  Movie.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 03/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import ObjectMapper

class MovieResponse: NSObject, Mappable {

    required convenience init?(map: Map) {
        self.init()
    }

    var vote_count:Int?
    var id: Int64?
    var video: Bool?
    var vote_average: Double?
    var title: String?
    var popularity: Double?
    var poster_path: String?
    var original_language: String?
    var original_title: String?
    var genre_ids: [Int]?
    var backdrop_path: String?
    var adult: Bool?
    var overview: String?
    var release_date: String?

    func mapping(map: Map){
        vote_count <- map["vote_count"]
        id <- map["id"]
        video <- map["video"]
        vote_average <- map["vote_average"]
        title <- map["title"]
        popularity <- map["popularity"]
        poster_path <- map["poster_path"]
        original_language <- map["video"]
        original_title <- map["original_title"]
        genre_ids <- map["genre_ids"]
        backdrop_path <- map["backdrop_path"]
        adult <- map["adult"]
        overview <- map["overview"]
        release_date <- map["release_date"]
    }
}
