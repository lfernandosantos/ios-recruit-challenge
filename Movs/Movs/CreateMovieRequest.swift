//
//  CreateMovieRequest.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 03/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import Alamofire

class CreateMovieRequest: NSObject {

    let key = "1dbb7d290ce2cb88ef8c311f67afd994"

    func fetchMovies(page: Int, completion: @escaping (Bool, Any?) -> Void) {

        let url = "https://api.themoviedb.org/3/movie/popular?api_key=\(key)&language=en-US&page=\(page)"

        Alamofire.request(url).responseJSON{ response in

            switch response.result {
            case .success:
                completion(true, response.result.value)
                break
            case .failure(let error):
                completion(false, error)
            }
        }
    }

    func fetchGenre(completion: @escaping (Bool, Any?) -> Void) {

        let url = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(key)"

        Alamofire.request(url).responseString { response in

            switch response.result {
            case .success:
                completion(true, response.result.value)
                break
            case .failure(let error):
                completion(false, error)
            }
        }
    }
}
