//
//  ViewController.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 03/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate{

    @IBOutlet var errorView: ErrorView!
    var movieList = [MovieResponse]()
    var viewModel: MovieListP?
    var isSearching = false
    var lastMovies: [MovieResponse]?

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionList: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        setupViews()

        viewModel = MovieListP(view: self)
        viewModel?.getMovies(more: false)

        //let tabRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        //view.addGestureRecognizer(tabRecognizer)

        collectionList.delegate = self
    }
    

    @objc func tapped(){
        view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "cel", for: indexPath) as! CollectionViewCell
        cel.titlelbl.text = movieList[indexPath.item].title
        let posterPath = movieList[indexPath.item].poster_path

        if let path = posterPath {
            let poster = URL(string:"https://image.tmdb.org/t/p/w500/\(path)")
            cel.movieImg.sd_setImage(with: poster, placeholderImage: UIImage(named: "place_holder.png"))
        } else {
            cel.movieImg.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "place_holder.png"))
        }

        cel.layer.cornerRadius = 4
        cel.layer.borderWidth = 2
        cel.layer.borderColor = UIColor.gray.cgColor
        return cel
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detail", sender: movieList[indexPath.item])
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailViewController = segue.destination as? DetailsTableVC {
            if let movieToSend = sender as? MovieResponse {
                detailViewController.movieViewModel = MovieVM(movie: movieToSend)
            }
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !isSearching {
            self.viewModel?.getMovies(more: true)
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }

    func loadMovies(_ movies: [MovieResponse]) {
        if movies.count == 0 {
            if isSearching {
                errorView.errorLabel.text = "Ops!\nNenhum resultado para sua busca!"
                errorView.isHidden = false
                collectionList.isHidden = true
            } else {
                self.emptList()
            }
        } else {
            errorView.isHidden = true
            collectionList.isHidden = false
        }
        movieList = movies
        collectionList.reloadData()
        if lastMovies == nil {
            lastMovies = movieList
        }
    }

    func addMoreMovies(_ movies: [MovieResponse]) {
        for m in movies{
            movieList.append(m)
            collectionList.reloadData()
            lastMovies = movieList
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !errorView.isHidden {
            viewModel?.getMovies(more: false)
        }

        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            self.loadLastMovies()
        } else {
            print("true")
            isSearching = true
            searchMovies(text: searchBar.text!)
        }
    }

    func searchMovies(text: String) {
        var searchList = [MovieResponse]()
        movieList = [MovieResponse] ()
        var listToSearch: [MovieResponse]

        if let last = lastMovies {
            print("Last")
            listToSearch = last
        } else {
            listToSearch = movieList
        }
        for m in listToSearch {

            if (m.title?.contains(text))! {
                searchList.append(m)
                self.loadMovies(searchList)
            } else {
                self.loadMovies(searchList)
            }
        }
    }

    func loadLastMovies() {
        if let movies = lastMovies {
            loadMovies(movies)
        }
    }

    func setupViews() {
        errorView.center = self.view.center
        self.view.addSubview(errorView)
        errorView.isHidden = true
    }

    func emptList() {
        errorView.errorLabel.text = "Eita!\nProblemas com internet.\nVerifique sua conexão e tente novamente!"
        errorView.isHidden = false
        collectionList.isHidden = true
    }
}
