//
//  CollectionViewCell.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 04/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
}
