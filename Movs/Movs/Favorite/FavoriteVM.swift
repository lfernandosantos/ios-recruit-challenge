//
//  FavoriteVM.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 08/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class FavoriteVM {

    let id: Int64
    let title: String
    let image: UIImage
    let year: String

    init(movie: Favorite) {
        self.id = movie.id
        self.title = movie.title ?? "untitle"
        self.year = movie.releaseDate ?? "0000"

        if let imgPath = movie.posterPath {
            let poster = URL(string:"https://image.tmdb.org/t/p/w500/\(imgPath)")

            let imageView = UIImageView()
            imageView.sd_setImage(with: poster, placeholderImage: UIImage(named: "place_holder.png"))
            self.image = imageView.image ?? UIImage(named: "place_holder.png") ?? UIImage()
        } else {
            self.image = UIImage(named: "place_holder.png") ?? UIImage()

        }
    }
}
