//
//  FavoriteTableVC.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 08/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import CoreData

class FavoriteTableVC: UITableViewController {


    @IBOutlet var errorView: ErrorView!
    var favoriteList: [Favorite]?
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source



    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return favoriteList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoritos", for: indexPath) as! FavoriteTableViewCell

        guard let list = favoriteList else {
            return cell
        }

        if list.count > 0 {
            let item = list[indexPath.row]
            let favoriteViewModel = FavoriteVM(movie: item)

            cell.favoriteImg.image = favoriteViewModel.image
            cell.favoriteImg.layer.cornerRadius = 4
            cell.favoriteImg.layer.borderWidth = 2
            cell.favoriteImg.layer.borderColor = UIColor.gray.cgColor
            cell.favoriteTitleLabel.text = favoriteViewModel.title
            cell.favoriteYearLabel.text = favoriteViewModel.year
        }

        cell.layer.borderWidth = 2
        cell.layer.borderColor = UIColor.gray.cgColor

        return cell
    }


    override func viewDidAppear(_ animated: Bool) {
        loadFavoriteList()
    }

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let unFavorite = UITableViewRowAction(style: .destructive, title: "Remove") { action, index in

            if let item = self.favoriteList?[indexPath.row] {
                FavoriteDAO.deleteFavorite(id: item.id)
                self.loadFavoriteList()
            }
        }
        unFavorite.backgroundColor = .red

        return [unFavorite]
    }

    func loadFavoriteList() {
        let fetch: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        do {
            let favoritos = try PersistenceService.context.fetch(fetch)
            if favoritos.count > 0 {
                favoriteList = favoritos
                hiddenErroView()
                tableView.reloadData()
            } else {
                emptList()
            }
        } catch(let error) {
            print(error)

        }
        tableView.reloadData()
    }

    func setupViews() {
        errorView.center = self.view.center
        self.view.addSubview(errorView)
        errorView.isHidden = true
    }

    func hiddenErroView() {
        errorView.isHidden = true
        tableView.isHidden = false
    }
    func emptList() {
        errorView.errorLabel.text = "Você não possui favoritos!"
        errorView.isHidden = false
        tableView.separatorStyle = .none
        tableView.numberOfRows(inSection: 0)
    }
}
