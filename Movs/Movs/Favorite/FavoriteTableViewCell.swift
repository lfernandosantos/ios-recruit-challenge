//
//  FavoriteTableViewCell.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 08/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    @IBOutlet weak var favoriteImg: UIImageView!
    @IBOutlet weak var favoriteTitleLabel: UILabel!
    @IBOutlet weak var favoriteYearLabel: UILabel!
    
}
