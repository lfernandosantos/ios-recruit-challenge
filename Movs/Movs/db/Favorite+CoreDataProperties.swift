//
//  Favorite+CoreDataProperties.swift
//  
//
//  Created by Luiz Fernando dos Santos on 07/03/2018.
//
//

import Foundation
import CoreData


extension Favorite {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favorite> {
        return NSFetchRequest<Favorite>(entityName: "Favorite")
    }

    @NSManaged public var id: Int64
    @NSManaged public var video: Bool
    @NSManaged public var voteCount: Int64
    @NSManaged public var voteAverage: Double
    @NSManaged public var title: String?
    @NSManaged public var popularity: Double
    @NSManaged public var posterPath: String?
    @NSManaged public var originalTitle: String?
    @NSManaged public var genreIds: String?
    @NSManaged public var releaseDate: String?

}
