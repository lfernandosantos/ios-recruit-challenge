//
//  FavoriteDAO.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 08/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import Foundation
import CoreData

class FavoriteDAO {

    static func deleteFavorite(id: Int64) {
        let context = PersistenceService.context
        let fetch: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        fetch.predicate = NSPredicate(format: "id = %@", "\(id)")

        do {
            if let favoritos: [Favorite] = try context.fetch(fetch) {
                for object in favoritos {
                    context.delete(object)
                    print(object.title)
                    try context.save()
                    //todo: completion
                }
            }
        } catch { }
    }
}
