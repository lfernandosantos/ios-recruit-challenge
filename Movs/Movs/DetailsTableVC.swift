//
//  DetailsVC.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 06/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import CoreData

class DetailsTableVC: UITableViewController {

    var movieViewModel: MovieVM!

    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var moviePrincipalImage: UIImageView!
    @IBOutlet weak var movieDescriptionText: UITextView!
    @IBOutlet weak var favoriteButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()

        moviePrincipalImage.image = movieViewModel.imageMovie
        movieTitleLabel.text = movieViewModel.titleMovie
        movieYearLabel.text = movieViewModel.releaseData
        movieGenre.text = movieViewModel.genreMovie
        movieDescriptionText.text = movieViewModel.descriptionMovie

        movieViewModel.imageMovie.getColors() { colors in
            self.view.backgroundColor = colors.background
            self.movieTitleLabel.textColor = colors.primary
            self.tableView.backgroundColor = colors.background
            self.movieYearLabel.textColor = colors.primary
            self.movieGenre.textColor = colors.primary
            self.movieDescriptionText.textColor = colors.primary

            let gradient = CAGradientLayer()
            let colorTop = UIColor.clear.cgColor
            let colorBottom = colors.background.cgColor
            gradient.colors = [colorTop, colorBottom]
            gradient.frame = self.moviePrincipalImage.bounds
            self.moviePrincipalImage.layer.insertSublayer(gradient, at: 0)
        }

        if let rect = self.navigationController?.navigationBar.frame {
            self.tableView.contentInset = UIEdgeInsetsMake(-rect.size.height, 0, 0, 0)
        }

        favoriteButton.setImage(UIImage(named: "ic_favorited.png"), for: .selected)

        let fetch: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        fetch.predicate = NSPredicate(format: "id = %@", "\(movieViewModel.idMovie)")

        do {
            let favoritos = try PersistenceService.context.fetch(fetch)
            if favoritos.count > 0 {
                favoriteButton.isSelected = true
            }

        } catch { }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func FavoriteMovie(_ sender: Any) {
        if favoriteButton.isSelected {

            let context = PersistenceService.context
            let fetch: NSFetchRequest<Favorite> = Favorite.fetchRequest()
            fetch.predicate = NSPredicate(format: "id = %@", "\(movieViewModel.idMovie)")

            do {
                if let favoritos: [Favorite] = try context.fetch(fetch) {
                    for object in favoritos {
                        context.delete(object)
                        print(object.title)
                        try context.save()
                        favoriteButton.isSelected = false
                    }

                }

            } catch { }
        } else {
            let favorite = Favorite(context: PersistenceService.context)
            favorite.title = movieViewModel.titleMovie
            favorite.posterPath = movieViewModel.posterPath
            favorite.releaseDate = movieViewModel.releaseData
            favorite.id = movieViewModel.idMovie
            PersistenceService.saveContext()
            favoriteButton.isSelected = true
        }
    }

    func isFavorite(id: Int64, completion: (Bool) -> Void) {
        let fetch: NSFetchRequest<Favorite> = Favorite.fetchRequest()
        fetch.predicate = NSPredicate(format: "id = %@", "\(id)")

        do {
            let favoritos = try PersistenceService.context.fetch(fetch)
            if favoritos.count > 0 {
                completion(true)
            }
            completion(false)

        } catch(let error) {
            print(error)
            completion(false)
        }
    }

}
