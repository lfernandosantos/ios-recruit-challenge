//
//  GenreRequest.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 09/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import ObjectMapper
import UIKit

class GenreResponse: Mappable {

    var genres: [Genre]?

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map){
        genres <- map["genres"]
    }
        
}
