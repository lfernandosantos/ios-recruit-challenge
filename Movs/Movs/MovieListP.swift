//
//  MovieListVM.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 03/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit

class MovieListP: NSObject {

    let view: ViewController
    var requestResult: MovieListResponse?

    init(view: ViewController) {
        self.view = view
    }

    func getMovies(more: Bool) {
        if more{
            guard let currentPage = requestResult?.page else {
                print("currentpage nil")
                return
            }
            guard let totalPages = requestResult?.totalPages else {
                 print("total nil")
                return
            }
            if currentPage < totalPages {
                CreateMovieRequest().fetchMovies(page: currentPage + 1){ success, result in
                    if success {
                        guard let request = result as? [String: Any] else {
                            print("Erro converter request")
                            return
                        }
                        self.requestResult = MovieListResponse(JSON: request)
                        if let movies = self.requestResult?.results{
                            self.view.addMoreMovies(movies)
                        } else {
                            self.view.emptList()
                        }
                    } else {
                        self.view.emptList()
                    }
                }
            }
        } else {
            CreateMovieRequest().fetchMovies(page: 1){ success, result in
                if success {
                    guard let request = result as? [String: Any] else {
                        print("Erro converter request")
                        return
                    }
                    self.requestResult = MovieListResponse(JSON: request)
                    if let movies = self.requestResult?.results{
                        self.view.loadMovies(movies)
                    } else {
                        self.view.emptList()
                    }
                } else {
                    self.view.emptList()
                }
            }
        }
    }


}
