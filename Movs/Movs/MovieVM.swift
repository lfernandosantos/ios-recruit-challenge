//
//  MovieVM.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 06/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import SDWebImage

public final class MovieVM {

    let movie: MovieResponse
    let idMovie: Int64
    let imageMovie: UIImage
    let titleMovie: String
    let posterPath: String
    let releaseData: String
    var genreMovie: String
    let descriptionMovie: String

    init(movie: MovieResponse) {
        self.movie = movie

        if let imgPath = movie.poster_path {
            let poster = URL(string:"https://image.tmdb.org/t/p/w500/\(imgPath)")

            let imageView = UIImageView()
            imageView.sd_setImage(with: poster, placeholderImage: UIImage(named: "place_holder.png"))
            self.imageMovie = imageView.image ?? UIImage(named: "place_holder.png") ?? UIImage()
        } else {
            self.imageMovie = UIImage(named: "place_holder.png") ?? UIImage()

        }
        self.titleMovie = movie.title ?? "No title."
        self.posterPath = movie.poster_path ?? ""
        self.idMovie = movie.id!
        self.releaseData = movie.release_date ?? "0000"
        self.genreMovie = ""
        self.descriptionMovie = movie.overview ?? "No overview."

        getGenreIds { genreStr in
            self.genreMovie = genreStr
        }
    }

    func getGenreIds(completion:@escaping (String) -> Void){

        var stringGenres = ""
        CreateMovieRequest().fetchGenre { sucess, result in

            if sucess {
                guard let response = result as? String else {
                    print("Erro converter request")
                    return
                }

                guard let movieGenreIds = self.movie.genre_ids else {
                    return
                }

                let requestResult = GenreResponse(JSONString: response)
                if let genreIds = requestResult?.genres {

                    for movieGenre in movieGenreIds {
                        for genre in genreIds{
                            if movieGenre == genre.id! {
                                stringGenres += " \(genre.name!)"
                            }
                        }
                    }
                    completion(stringGenres)
                }
            }
        }
    }

    func getBlurImage() -> UIImage {
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: imageMovie)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(10, forKey: kCIInputRadiusKey)

        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")

        let context = CIContext(options: nil)
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage
    }

}
