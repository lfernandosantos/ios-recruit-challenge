//
//  MovieListRequest.swift
//  Movs
//
//  Created by Luiz Fernando dos Santos on 03/03/2018.
//  Copyright © 2018 LFSantos. All rights reserved.
//

import UIKit
import ObjectMapper

class MovieListResponse: NSObject, Mappable {

    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var results: [MovieResponse]?

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        page <- map["page"]
        totalPages <- map["total_results"]
        totalPages <- map["total_pages"]
        results <- map["results"]
    }

}
